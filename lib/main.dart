import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:simple_animations/simple_animations/multi_track_tween.dart';
import 'package:sqflite/sqflite.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:page_transition/page_transition.dart';
//IDIOMAS
List<String> gblIdiomas = [
  "中国人",
  "English",
  "Português",
  "Espanol",
];

//IDIOMAS
int indiceIdioma = 2; //0-Chines //1-Ingles //2-Português //3-Espanhol
List<Map<String, dynamic>> gblIdiomasIndice = [
  //chines
  { "perfil": "轮廓"    , 
    "catalogo": "查敦的目录", 
    "youtube": "YouTube视频", 
    "cross": "下载参考", 
    "idioma": "语言能力", 
    "lingua": "中国人",
    "perfil_nome":"名",
    "perfil_email":"电邮地址",
    "perfil_empresa":"公司名称",
    "perfil_senha":"密码",
    "perfil_confirmacao": "确认书",
    "procurar":"搜寻",
    "tituloProdutos": "Chardon 产品展示",
    "videosTitulo":"Chardon 影片",
    "videoDescricao1":"(4:08) 機構影片",
    "videoDescricao2":"(5:55) 安裝 CODO OCC",
    "videoDescricao3":"(4:53) 車身安裝 T",
    "videoDescricao4":"(6:48) 安裝與操作",
    "videoDescricao5":"(7:11) TDC 安裝與操作",
    "videoIdioma1":"葡萄牙文",
    "videoIdioma2":"西班牙文",
    "videoIdioma3":"葡萄牙文",
    "videoIdioma4":"葡萄牙文",
    "videoIdioma5":"葡萄牙文",
    "tituloPaginalInicial": "查敦集团",
    "bemVindo":"欢迎",
    "bemVindoL1": "您将获得的最佳连接器",
    "bemVindoL2": "在Chardon的目录中找到",
    "produtosCategorias":"按类别",
    "categoria1":"IEEE/ANSI 200A Loadbreak",
    "categoria2":"IEEE/ANSI 200A Deadbreak",
    "categoria3":"IEEE/ANSI 600A Deadbreak",
    "categoria4":"IEC Acessórios Cabo-A",
    "categoria5":"IEC Acessórios Cabo-B",
    "categoria6":"IEC Acessórios Cabo-C",
    "categoria7":"Componentes de Transformadores",
    "categoria8":"Produtos Epóxi",
    "categoria9":"Religadores",
    "categoria10":"Produtos termoencolhíveis a frio",
    "categoria11":"其他产品",
    "categoriaTodos":"全部商品",
    "idiomaPreferencia" : "选择语言",
  },
  //ingles
  { "perfil": "Profile" , 
    "catalogo": "Chardon's Catalog",
    "youtube": "Youtube Medias",
    "cross": "Download Cross References", 
    "idioma": "Language", 
    "lingua": "Ensglish",
    "perfil_nome":"Name",
    "perfil_email":"Email",
    "perfil_empresa":"Company",
    "perfil_senha":"Password",
    "perfil_confirmacao": "Confirmation",
    "procurar":"Search",
    "tituloProdutos": "Chardon Products",
    "videosTitulo":"Chardon Medias",
    "videoDescricao1":"(4:08) Institutional Video",
    "videoDescricao2":"(5:55) Instalation Code OCC",
    "videoDescricao3":"(4:53) Instalation T Body",
    "videoDescricao4":"(6:48) Instalation and Operation",
    "videoDescricao5":"(7:11) TDC Instalation and Operation",
    "videoIdioma1":"Portuguese",
    "videoIdioma2":"Spanish",
    "videoIdioma3":"Portuguese",
    "videoIdioma4":"Portuguese",
    "videoIdioma5":"Portuguese",
    "tituloPaginalInicial": "Chardon Group",
    "bemVindo":"Welcome",
    "bemVindoL1": "The best connectores you will",
    "bemVindoL2": "find in Chardon's catalogs",
    "produtosCategorias":"By Categories",
    "categoria1":"IEEE/ANSI 200A Loadbreak",
    "categoria2":"IEEE/ANSI 200A Deadbreak",
    "categoria3":"IEEE/ANSI 600A Deadbreak",
    "categoria4":"IEC Cable Accessories - Interface A",
    "categoria5":"IEC Cable Accessories - Interface B",
    "categoria6":"IEC Cable Accessories - Interface C",
    "categoria7":"Transformer Components",
    "categoria8":"Epoxy Products",
    "categoria9":"Switcher Products",
    "categoria10":"Cold Shrinkable Products",
    "categoria11":"Other Products",
    "categoriaTodos":"All Products",
    "idiomaPreferencia" : "Select Language",

  },
  //portugues
  { "perfil": "Perfil"  , 
    "catalogo": "Catálogo da Chardon",
    "youtube": "Vídeos no Youtube",
    "cross": "Download Referências", 
    "idioma": "Idioma", 
    "lingua": "Português",
    "perfil_nome":"Nome",
    "perfil_email":"Email",
    "perfil_empresa":"Empresa",
    "perfil_senha":"Senha",
    "perfil_confirmacao": "Confirmação",
    "procurar":"Procurar",
    "tituloProdutos": "Produtos Chardon",
    "videosTitulo":"Vídeos da Chardon",
    "videoDescricao1":"(4:08) Vídeo Institucional",
    "videoDescricao2":"(5:55) Instalação do CODO OCC",
    "videoDescricao3":"(4:53) Instalação do Corpo T",
    "videoDescricao4":"(6:48) Instalação e Operação",
    "videoDescricao5":"(7:11) TDC Instalção e Operação",
    "videoIdioma1":"Português",
    "videoIdioma2":"Espanhol",
    "videoIdioma3":"Portugês",
    "videoIdioma4":"Portugês",
    "videoIdioma5":"Portugês",
    "tituloPaginalInicial": "Grupo Chardon",
    "bemVindo":"Bem Vindo",
    "bemVindoL1": "Os melhores conectores você vai",
    "bemVindoL2": "encontrar nos catálogos da Chardon",
    "produtosCategorias":"Por Categorias",
    "categoria1":"IEEE/ANSI 200A Loadbreak",
    "categoria2":"IEEE/ANSI 200A Deadbreak",
    "categoria3":"IEEE/ANSI 600A Deadbreak",
    "categoria4":"IEC Acessórios Cabo-A",
    "categoria5":"IEC Acessórios Cabo-B",
    "categoria6":"IEC Acessórios Cabo-C",
    "categoria7":"Componentes de Transformadores",
    "categoria8":"Produtos Epóxi",
    "categoria9":"Religadores",
    "categoria10":"Produtos termoencolhíveis a frio",
    "categoria11":"Outros Produtos",
    "categoriaTodos":"Todos os Produtos",
    "idiomaPreferencia" : "Selecione Idioma",

  },
  //espanhol
  { "perfil": "Perfil"  , 
    "catalogo": "Catálogo de Chardon",
    "youtube": "Videos de Youtube",
    "cross": "Descargar Referencias", 
    "idioma": "Idioma", 
    "lingua": "Espanol",
    "perfil_nome":"Nombre",
    "perfil_email":"Correo electrónico",
    "perfil_empresa":"Empresa",
    "perfil_senha":"Contraseña",
    "perfil_confirmacao": "Confirmación",
    "procurar":"Buscar",
    "tituloProdutos": "Productos Chardon",
    "videosTitulo":"Videos de Chardon",
    "videoDescricao1":"(4:08) Video institucional",
    "videoDescricao2":"(5:55) Instalación de CODO OCC",
    "videoDescricao3":"(4:53) Instalación de Corpo T",
    "videoDescricao4":"(6:48) Instalación y Operación",
    "videoDescricao5":"(7:11) TDC Instalación y Operación",
    "videoIdioma1":"Portugués",
    "videoIdioma2":"Espanol",
    "videoIdioma3":"Portugués",
    "videoIdioma4":"Portugués",
    "videoIdioma5":"Portugués",
    "tituloPaginalInicial": "Grupo Chardon",
    "bemVindo":"Bienvenido",
    "bemVindoL1": "Los mejores conectadores que ",
    "bemVindoL2": "encontrarás en los catálogos de Chardon",
    "produtosCategorias":"Por categorías",
    "categoria1":"IEEE/ANSI 200A Loadbreak",
    "categoria2":"IEEE/ANSI 200A Deadbreak",
    "categoria3":"IEEE/ANSI 600A Deadbreak",
    "categoria4":"IEC Accesorios de Cable-A",
    "categoria5":"IEC Accesorios de Cable-B",
    "categoria6":"IEC Accesorios de Cable-C",
    "categoria7":"Componentes del transformador",
    "categoria8":"Productos epóxicos",
    "categoria9":"Reconectadores",
    "categoria10":"Productos termocontraíbles fríos",
    "categoria11":"Otros Produtos",
    "categoriaTodos":"Todos los productos",
    "idiomaPreferencia" : "Seleccionar idioma",
  },
];

  loadPDFFromUrl(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }  

void main() => runApp(MyApp());

class ItemChardon {
  String id;
  String categoria;
  String descricao;
  String partnumber;
  String campopesquisa;
  String assetImagem;
  String assetImagemMini;
  String catalogo;
  String instrucao;
  String video;

  //Constructor
  ItemChardon ({
    this.id,
    this.categoria,
    this.descricao,
    this.partnumber,
    this.campopesquisa,
    this.assetImagem,
    this.assetImagemMini,
    this.catalogo,
    this.instrucao,
    this.video,
  });

  ItemChardon.fromJason(Map<String, dynamic> json) {
    id              = json["id"];
    categoria       = json["categoria"];
    descricao       = json["descricao"];
    partnumber      = json["partnumber"];
    campopesquisa   = json["campopesquisa"];
    assetImagem     = json["assetImagem"];
    assetImagemMini = json["assetImagemMini"];
    catalogo        = json["catalogo"];
    instrucao       = json["instrucao"];
    video           = json["video"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["id"]              = this.id;
    data["categoria"]       = this.categoria;
    data["descricao"]       = this.descricao;
    data["partnumber"]      = this.partnumber;
    data["campopesquisa"]   = this.campopesquisa;
    data["assetImagem"]     = this.assetImagem;
    data["assetImagemMini"] = this.assetImagemMini;
    data["catalogo"]        = this.catalogo;
    data["instrucao"]       = this.instrucao;    
    data["video"]           = this.video;    
    return data;
  }
}
//PAGINA
class Imagem extends StatelessWidget {
  Imagem(this.imagem, this.descricao);
  final String imagem;
  final String descricao;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.descricao),
      ),
      body: Center(
        child: Image.asset(this.imagem),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chardon Group Catalog',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: <String, WidgetBuilder>{
        "/": (BuildContext context) => new MyHomePage(title: gblIdiomasIndice[indiceIdioma]["tituloPaginalInicial"]),
      //  "/menuprodutos": (BuildContext context) => new MenuProdutos(),
        "/listavideos": (BuildContext context) => new ListaVideos(),
        "/produtosCategias0": (BuildContext context) => new ListaProdutos("0"),
        "/produtosCategias1": (BuildContext context) => new ListaProdutos("1"),
        "/produtosCategias2": (BuildContext context) => new ListaProdutos("2"),
        "/produtosCategias3": (BuildContext context) => new ListaProdutos("3"),
        "/produtosCategias4": (BuildContext context) => new ListaProdutos("4"),
        "/produtosCategias5": (BuildContext context) => new ListaProdutos("5"),
        "/produtosCategias6": (BuildContext context) => new ListaProdutos("6"),
        "/produtosCategias7": (BuildContext context) => new ListaProdutos("7"),
        "/produtosCategias8": (BuildContext context) => new ListaProdutos("8"),
        "/produtosCategias9": (BuildContext context) => new ListaProdutos("9"),
        "/produtosCategias10": (BuildContext context) => new ListaProdutos("10"),
        "/produtosCategias11": (BuildContext context) => new ListaProdutos("11"),
        "/idioma": (BuildContext context) => new SelecionaIdioma(),
      //  "/PDF" : (BuildContext context) => new PDF(),
        "/perfil" : (BuildContext context) => new Perfil(),
        "/starter" : (BuildContext context) => new Starter(),
        
      },
      initialRoute: "/",
      //home: MyHomePage(title: 'Chardon Group'),
    );
  }
}


class CustomListTile extends StatelessWidget{
  //construtor para poder passar parâmetros ao widget
  final IconData icon;
  final String description;
  final Function onTap;

  CustomListTile(this.icon, this.description, this.onTap);
  
  @override
  Widget build(BuildContext context){
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey[300])),
        ),
        child: InkWell(
          splashColor: Colors.blueAccent,
          onTap: onTap,
          child: Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(icon),
                    Padding(
                      padding: const EdgeInsets.all(8),
                      child: Text(description, style: TextStyle(fontSize: 16, ),),
                    ),
                  ],
                ),
                Icon(Icons.arrow_right),
              ],
            ),
          ),
        )
      ),
    );
  }
}


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int meuIndice;

  @override
  void initState() {
    _executar();
    super.initState();
  }

  void _executar() async{
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { },
    onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE cfg (idioma integer)");
      await db.execute("CREATE TABLE profile (nome TEXT, email TEXT, empresa TEXT, senha TEXT)");
    });

    //DELETE
    //await database.rawDelete(
    //  'DELETE FROM cfg',
    //);

    //INSERT
    //await database.rawInsert(
    //  'INSERT INTO cfg (idioma) values (?)', [1]
    //);        

    //SELECT
    var lista = await database.query("cfg", 
      columns: ["idioma"],
      where: "idioma>=?",
      whereArgs: ["0"]
    );    
    if(lista.length == 0){
      await database.rawInsert(
        'INSERT INTO cfg (idioma) values (?)', [1]
      );
      await database.rawInsert(
        'INSERT INTO profile (nome, email, empresa, senha) values ( ?, ?, ?, ?)', 
        ["", "", "", ""]
      );  
      setState(() {
        indiceIdioma = 1;
        meuIndice = 1;
      });      
    }    
    for(var item in lista){
      setState(() {
        indiceIdioma = item["idioma"];
        meuIndice = item["idioma"];
      });
    }
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: new Image.asset(
                'assets/images/logo.png',
              ),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
            ),
            CustomListTile(Icons.person, gblIdiomasIndice[indiceIdioma]["perfil"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/perfil"); }
            ),
            CustomListTile(Icons.apps, gblIdiomasIndice[indiceIdioma]["catalogo"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/starter"); }
            ),
            CustomListTile(Icons.video_library, gblIdiomasIndice[indiceIdioma]["youtube"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/listavideos"); }
            ),
            CustomListTile(Icons.file_download, gblIdiomasIndice[indiceIdioma]["cross"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/PDF"); }
            ),
            CustomListTile(Icons.tune, gblIdiomasIndice[indiceIdioma]["idioma"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/idioma"); }
            ),
          ],
        ),
      ),      
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/b$indiceIdioma.jpg',
            ),
            SizedBox(height: 40),
            
            Image.asset(
              'assets/images/back11.jpg',
            ),
            Text(
              '$_counter',
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          _incrementCounter();
          Navigator.of(context).pushNamed('/starter');
        },
        tooltip: 'Increment',
        child: Icon(Icons.arrow_forward_ios),
      ),
    );
  }
}

//PÁGINA DE VÍDEOS DISPONÍVEIS CHARDON
class ListaVideos extends StatefulWidget {

  @override
  _ListaVideosState createState() => _ListaVideosState();
}
class _ListaVideosState extends State<ListaVideos> {

  Widget _listVideos(BuildContext context, String url, String groupName, String subGroupName, String otherData, String index){
    return Card( 
      child: Padding(
        padding: EdgeInsets.only(left: 0, right: 0, top: 20),
        child: InkWell(  
          onTap: () {
            _launchVideo(url);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Icon(Icons.play_arrow),
                    SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children:<Widget>[
                        Text(
                          groupName, 
                          style: TextStyle(
                            fontFamily: "Arial",
                            fontSize: 17,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        Text(
                          subGroupName, 
                          style: TextStyle(
                            fontFamily: "Arial",
                            fontSize: 17,
                          ),
                        ),
                        Text(
                          otherData, 
                          style: TextStyle(
                            fontFamily: "Arial",
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  _launchVideo(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(gblIdiomasIndice[indiceIdioma]["videosTitulo"]),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _listVideos(context, 'https://www.youtube.com/watch?v=cfVtJkZXQkA&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn', gblIdiomasIndice[indiceIdioma]["videoDescricao1"], 'PT 2018 Chardon', gblIdiomasIndice[indiceIdioma]["videoIdioma1"], '1'),
            _listVideos(context, 'https://www.youtube.com/watch?v=X-3_ksuNL_o&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn', gblIdiomasIndice[indiceIdioma]["videoDescricao2"], '15/25kV 200A Chardon',gblIdiomasIndice[indiceIdioma]["videoIdioma2"], '2'),
            _listVideos(context, 'https://www.youtube.com/watch?v=x1BGVFD4kiQ&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn', gblIdiomasIndice[indiceIdioma]["videoDescricao3"], '15/25kV 600A Chardon', gblIdiomasIndice[indiceIdioma]["videoIdioma3"], '3'),
            _listVideos(context, 'https://www.youtube.com/watch?v=r1upfG1c8OI&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn', gblIdiomasIndice[indiceIdioma]["videoDescricao4"], '15/25kV 600A Chardon', gblIdiomasIndice[indiceIdioma]["videoIdioma4"], '4'),
            _listVideos(context, 'https://www.youtube.com/watch?v=xC5ysQid1Kc&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn', gblIdiomasIndice[indiceIdioma]["videoDescricao5"], '15/25kV 600A Chardon', gblIdiomasIndice[indiceIdioma]["videoIdioma5"], '5'),
          ],
        ),
      ),
    );
  }
}
// FIM PAGINA DE VIDEOS CHARDON


//PÁGINA DE VÍDEOS DISPONÍVEIS CHARDON
class ListaProdutosCategorias extends StatefulWidget {
  @override
  _ListaProdutosCategoriasState createState() => _ListaProdutosCategoriasState();
}
class _ListaProdutosCategoriasState extends State<ListaProdutosCategorias> {

  Widget _listCategorias(BuildContext context,String groupName, String index){
    return Card( 
      child: Padding(
        padding: EdgeInsets.only(left: 0, right: 0, top: 20),
        child: InkWell(  
          onTap: () {
            Navigator.of(context).pushNamed("/produtosCategias$index");
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Icon(Icons.arrow_right),
                    SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children:<Widget>[
                        Text(
                          groupName, 
                          style: TextStyle(
                            fontFamily: "Arial",
                            fontSize: 17,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(gblIdiomasIndice[indiceIdioma]["produtosCategorias"]),
      ),
      body: ListView(
        children: <Widget>[
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria1"], '1'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria2"], '2'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria3"], '3'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria4"], '4'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria5"], '5'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria6"], '6'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria7"], '7'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria8"], '8'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria9"], '9'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria10"], '10'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoria11"], '11'),
            _listCategorias(context, gblIdiomasIndice[indiceIdioma]["categoriaTodos"], '0'),
        ],
      )
    );
  }
}
// FIM PAGINA DE VIDEOS CHARDON




class ListaProdutos extends StatefulWidget {
  final String indice;
  ListaProdutos(this.indice);

  @override
  _ListaProdutosState createState() => _ListaProdutosState();
}
class _ListaProdutosState extends State<ListaProdutos> {
  List<ItemChardon> _notes = List<ItemChardon>();
  List<ItemChardon> _notesForDisplay = List<ItemChardon>();

  var itemsChardom = new List<ItemChardon>();

  Future<List<ItemChardon>> fetchNotes() async {
    var notes = _listaProdutos(widget.indice);
    return notes;
  }

  @override
  void initState() {
    fetchNotes().then((value) {
      setState(() {
        _notes.addAll(value);
        _notesForDisplay = _notes;
      });
    });
    super.initState();
  }  

  _listaProdutos(String indice) {

    int posicao = 0;
    int pagina = 1 ;
    String categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    itemsChardom = [];
    //1
    posicao++;
    if(indice=="0" || indice=="1") {
      itemsChardom.add(
        ItemChardon( 
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria" ,
        descricao       : "15 kV 200A Loadbreak Elbow TDC",
        partnumber      : "15-LE200",
        campopesquisa   : "15-LE200 165LR-C-5250 LE215 215LE ELB-15-210 IEEE/ANSI 200A Loadbreak 15 kV 200A Loadbreak Elbow TDC",
        assetImagem     : "assets/products/P001/001.jpg",
        assetImagemMini : "assets/products/P001/mini/001.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15LE200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/15-25LE200-_Instruction_Sheet-20171128.pdf",     
        //catalogo        : "http://189.79.77.141/chardon/catalogs/P00$pagina/00$posicao.pdf",
        //instrucao       : "http://189.79.77.141/chardon/instructions/P00$pagina/00$posicao.pdf",     
        //catalogo        : assets,
        //instrucao       : ,
        video           : "https://www.youtube.com/watch?v=xC5ysQid1Kc&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn&index=5", 
      ));
      //2
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV 200A Bushing Insert PIS",
        partnumber      : "15-LBI200",
        campopesquisa   : "15-LBI200 1601A4 LBI215 215BI ELB-15-200-BI IEEE/ANSI 200A Loadbreak 15 kV 200A Bushing Insert PIS",
        assetImagem     : "assets/products/P001/002.jpg",
        assetImagemMini : "assets/products/P001/mini/002.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15LBI200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LBI200-INSTRUCTION_SHEET-20170719.pdf",
        video           : "", 
      ));
      //3
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV 200A Loadbreak Protective Cap RIB",
        partnumber      : "15-LIC200",
        campopesquisa   : "15-LIC200 160DR LPC215 215CI ELB-15-200-IC IEEE/ANSI 200A Loadbreak 15 kV 200A Loadbreak Protective Cap RIB",
        assetImagem     : "assets/products/P001/003.jpg",
        assetImagemMini : "assets/products/P001/mini/003.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15LIC200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LBI200-INSTRUCTION_SHEET-20170719.pdf",
        video           : "", 
      ));
      //4
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV 200A Grounding Elbow TDC-AT",
        partnumber      : "15-GLE200",
        campopesquisa   : "15-GLE200 160DRG GE215 T6003091 $categoria 15 kV 200A Grounding Elbow TDC-AT",
        assetImagem     : "assets/products/P001/00$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15GLE.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A7610540_15-25kV_GROUNDING_ELBOW_Instructions.pdf",
        video           : "", 
      )); 
      //5
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV 200A Loadbreak Junction BDX/BQX/BTX",
        partnumber      : "15-LJ200",
        campopesquisa   : "$categoria 15 kV 200A Loadbreak Junction BDX/BQX/BTX 15-LJ200 ",
        assetImagem     : "assets/products/P001/00$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15LJ200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A0721042-1_15_25kV_200A_JUNCTION_Instructions.pdf",
        video           : "", 
      )); 
      //6
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV 200A Feed-Thru Insert PID",
        partnumber      : "15-LFTI",
        campopesquisa   : "$categoria 15 kV 200A Feed-Thru Insert PID 15-LFTI200",
        assetImagem     : "assets/products/P001/00$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15LFTI.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A7910040_15-25_kV_200A_Rotatable_Feed-Thru_Insert_Instructions.pdf",
        video           : "", 
      )); 
      //7
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV 200A Loadbreak Fuse Elbow TDC-F",
        partnumber      : "15-LFE200",
        campopesquisa   : "$categoria 15 kV 200A Loadbreak Fuse Elbow TDC-F 15-LFE200 168FLR1 LFEP215TFEC 215FE",
        assetImagem     : "assets/products/P001/00$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15LJ200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/15-25kV_200A_Fuse_Elbow_Instruction_Sheet.pdf",
        video           : "https://www.youtube.com/watch?v=X-3_ksuNL_o&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn", 
      )); 
      //8
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV 200A Loadbreak Portable Feed Thru PBP",
        partnumber      : "15-LPFT200",
        campopesquisa   : "$categoria 15 kV 200A Loadbreak Portable Feed Thru PBP 15-LPFT200",
        assetImagem     : "assets/products/P001/00$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15LJ200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LFT200-INSTRUCTION_SHEET-20161117.pdf",
        video           : "", 
      )); 
      //9
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV 200A Insulated Standoff Bushing",
        partnumber      : "15-SOB200",
        campopesquisa   : "$categoria 15 kV 200A Insulated Standoff Bushing 15-SOB200",
        assetImagem     : "assets/products/P001/00$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15SOB200.pdf",
        instrucao       : "",
        video           : "", 
      )); 
      //10
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV 200A Loadbreak Elbow TDC",
        partnumber      : "25-LE200",
        campopesquisa   : "$categoria 25 kV 200A Loadbreak Elbow TDC 25-LE200",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25LE200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/15-25LE200-_Instruction_Sheet-20171128.pdf",
        video           : "https://www.youtube.com/watch?v=xC5ysQid1Kc&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn", 
      )); 
      //11
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV 200A Bushing Insert PIS",
        partnumber      : "25-LBI200",
        campopesquisa   : "$categoria 25 kV 200A Bushing Insert PIS 25-LBI200 2701A4 LBI225 225BI ELB-25-200-BI",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25LBI200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LBI200-INSTRUCTION_SHEET-20170719.pdf",
        video           : "", 
      )); 
      //12
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV 200A Loadbreak Protective Cap RIB",
        partnumber      : "25-LIC200",
        campopesquisa   : "$categoria 25 kV 200A Loadbreak Protective Cap RIB 25-LIC200 ELB-25-200-IC",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25LIC200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A1020042-1-15-25kV_200A_INSULATING_CAP_Instructions.pdf",
        video           : "", 
      )); 
      //13
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV 200A  Grounding Elbow",
        partnumber      : "25-GLE200",
        campopesquisa   : "$categoria 25 kV 200A  Grounding Elbow 25-GLE200 273DRG LPC225 9U01BEW500",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15GLE.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A7610540_15-25kV_GROUNDING_ELBOW_Instructions.pdf",
        video           : "", 
      )); 
      //14
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV 200A Loadbreak Junction 3 or 5 Way (SSBracket)",
        partnumber      : "25-LJ200",
        campopesquisa   : "$categoria 25 kV 200A Loadbreak Junction 3 or 5 Way (SSBracket) 25-LJ200 25-LJ200F3 25-LJ200F4 274J3 274J4 LJ215C4B 228J3B 228J4B",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/25LJ200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A0721042-1_15_25kV_200A_JUNCTION_Instructions.pdf",
        video           : "", 
      )); 
      //15
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV 200A Feed-Thru Insert",
        partnumber      : "25-LFTI",
        campopesquisa   : "$categoria 25 kV 200A Feed-Thru Insert 25-LFTI",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25LFT1200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A7910040_15-25_kV_200A_Rotatable_Feed-Thru_Insert_Instructions.pdf",
        video           : "", 
      )); 
      //16
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV Loadbreak Fuse Elbow",
        partnumber      : "25-LFE200",
        campopesquisa   : "$categoria 25 kV Loadbreak Fuse Elbow 25-LFE200 274FLR1 LFEP228TFEC 228FE",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/25LFE200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/15-25kV_200A_Fuse_Elbow_Instruction_Sheet.pdf",
        video           : "https://www.youtube.com/watch?v=X-3_ksuNL_o&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn", 
      )); 
      //17
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV 200A Loadbreak Portable Feed Thru	",
        partnumber      : "25-LPFT200",
        campopesquisa   : "$categoria 25 kV 200A Loadbreak Portable Feed Thru	25-LPFT200",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15LPFT.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LFT200-INSTRUCTION_SHEET-20161117.pdf",
        video           : "", 
      )); 
      //18
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV 200A Insulated Stanfoff Bushing",
        partnumber      : "25-SOB200",
        campopesquisa   : "$categoria 25 kV 200A Insulated Stanfoff Bushing 25-SOB200",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25SOB200.pdf",
        instrucao       : "",
        video           : "", 
      )); 
      //19
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "Shield Adapter",
        partnumber      : "SADP",
        campopesquisa   : "$categoria Shield Adapter SADP",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/SADP.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/SADP-INSTRUCTION_SHEET-20161117.pdf",
        video           : "", 
      )); 
      //20
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15kV and 25kV Elbow Arrester",
        partnumber      : "15/25-LEA",
        campopesquisa   : "$categoria 15kV and 25kV Elbow Arrester 15-LEA 25-LEA 15/25-LEA",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/1525LEA.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LEA-INSTRUCTION_SHEET-20161123.pdf",
        video           : "", 
      )); 
      //21
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "35 kV 200A Loadbreak Elbow-Large Interface",
        partnumber      : "35L-LE200",
        campopesquisa   : "$categoria 35 kV 200A Loadbreak Elbow-Large Interface 35L-LE200 LE235 236LE",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/35LE200.pdf",
        instrucao       : "",
        video           : "", 
      )); 
      //22
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "35 kV 200A Loadbreak Elbow",
        partnumber      : "35-LE200",
        campopesquisa   : "$categoria 35 kV 200A Loadbreak Elbow 35-LE200 376LR",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/35LE200.pdf",
        instrucao       : "",
        video           : "", 
      )); 
      //23
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "35 kV 200A Loadbreak Bushing Insert",
        partnumber      : "35-LBI200",
        campopesquisa   : "$categoria 35 kV 200A Loadbreak Bushing Insert 35-LBI200 3701A3",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/35LBI200.pdf",
        instrucao       : "",
        video           : "", 
      )); 
      //24
      posicao++;
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "35 kV 200A Loadbreak Protective Cap",
        partnumber      : "35-LIC200",
        campopesquisa   : "$categoria 35 kV 200A Loadbreak Protective Cap 35-LIC200",
        assetImagem     : "assets/products/P001/0$posicao.jpg",
        assetImagemMini : "assets/products/P001/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/35LIC200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/25DE200-INSTRUCTION_SHEET-20161117.pdf",
        video           : "", 
      )); 
    }
    posicao=0;

    //PÁGINA 2
    //001
    pagina=2;
    posicao++;
    //categoria = "IEEE/ANSI 200A Deadbreak";
    categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    if(indice=="0" || indice == "2")
    {
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV, 200A Deadbreak Elbow",
        partnumber      : "25-DE200",
        campopesquisa   : "$categoria 25 kV, 200A Deadbreak Elbow 25-DE200",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ADEADBREAKCTCONLY/25DE200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/25DS200-INSTRUCTION_SHEET-20161117.pdf",
        video           : "", 
      )); 
      //002
      pagina=2;
      posicao++;
      //categoria = "IEEE/ANSI 200A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV, 200A Deadbreak Straight",
        partnumber      : "25-DS200",
        campopesquisa   : "$categoria 25 kV, 200A Deadbreak Straight 25-DS200",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ADEADBREAKCTCONLY/25DS200.pdf",
        instrucao       : "",
        video           : "", 
      )); 
      //003
      pagina=2;
      posicao++;
      //categoria = "IEEE/ANSI 200A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV, 200A Deadbreak Bushing Insert",
        partnumber      : "25-DBI200",
        campopesquisa   : "$categoria 25 kV, 200A Deadbreak Bushing Insert 25-DBI200",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ADEADBREAKCTCONLY/25DBI200.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/25DBI200-INSTRUCTION_SHEET-20161123.pdf",
        video           : "", 
      )); 
      //004
      pagina=2;
      posicao++;
      //categoria = "IEEE/ANSI 200A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25kV 200A Insulated Protective Cap",
        partnumber      : "25-DIC200",
        campopesquisa   : "$categoria 25kV 200A Insulated Protective Cap 25-DIC200",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ADEADBREAKCTCONLY/25DIC200.pdf",
        instrucao       : "",
        video           : "", 
      )); 
    }
    posicao = 0;


    //PAGINA 3 001
    pagina=3;
    posicao++;
    //categoria = "IEEE/ANSI 600A Deadbreak";
    categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    if(indice=="0" || indice == "3")
    {
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15/25 kV, 600A / 900A, T-Body",
        partnumber      : "15/25-TB600",
        campopesquisa   : "$categoria 15/25 kV, 600A / 900A, T-Body 15/25-TB600T 15-TB600T 25-TB600T K655LR-W0X BT625T 625TBT ELB-15/28-600T ELB-15/28-610",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/1525TB600.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/25-TBODY-INSTALLATION_SHEET-170921.pdf",
        video           : "https://www.youtube.com/watch?v=r1upfG1c8OI&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn", 
      )); 
      //PAGINA 3 002
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15/25 kV, 600A Insulated Protective Cap",
        partnumber      : "25-DIC600",
        campopesquisa   : "$categoria 15/25 kV, 600A Insulated Protective Cap 25-DIC600",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25DIC600.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A1040051_15_25_35kV__600A_INSULATING_CAP_Instruction.pdf",
        video           : "", 
      )); 
      //PAGINA 3 003
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15/25 kV, 600A Deadbreak Junction",
        partnumber      : "25-DJ600",
        campopesquisa   : "$categoria 15/25 kV, 600A Deadbreak Junction 25-DJ600F2 K650J2 JBI25C2B 625J2 ELB-35-600-J2-AL-STD",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25DJ600.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A0740041_25-35kV_600A_JUNCTION_Instructions.pdf",
        video           : "", 
      )); 
      //PAGINA 3 004
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15/25 kV, 600A Standoff Bushing",
        partnumber      : "25-SOB600",
        campopesquisa   : "$categoria 15/25 kV, 600A Standoff Bushing 25-SOB600",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25SOB.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A0420050_15_25kV_600-900A_STANDOFF_BUSHING_Instructions.pdf",
        video           : "", 
      )); 
      //PAGINA 3 005
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV Elbow/Load Reducing Tap Plug",
        partnumber      : "25-ETP600-15-200 25-LRTP600-15-200",
        campopesquisa   : "$categoria 15 kV Elbow/Load Reducing Tap Plug 25-ETP600-15-200 25-LRTP600-15-200 650ETP BLRTP615 615ETP ELB-15/28-200-ETP15 650LT-B LRTP615 615LRTP ELB-15/28-200-LRTP15",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25-15-ETP-LRTP.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A1620050-15-25kV_600A_ETP__Insutructions.pdf",
        video           : "", 
      )); 
      //PAGINA 3 006
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV Elbow/Load Reducing Tap Plug",
        partnumber      : "25-ETP600-25-200 25-LRTP600-25-200",
        campopesquisa   : "$categoria 25 kV Elbow/Load Reducing Tap Plug 25-ETP600-15-200 25-LRTP600-15-200 K650ETP BLRTP625 625ETP ELB-15/28-200-ETP25 K650LT-B LRTP625 625LRTP ELB-15/28-200-LRTP25",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25-25ETP-LRTP.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A1620050-15-25kV_600A_ETP__Insutructions.pdf",
        video           : "", 
      )); 
      //PAGINA 3 007
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "Multi Point Junction",
        partnumber      : "MPJ",
        campopesquisa   : "$categoria Multi Point Junction MPJ 628TM",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/MPJ.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A3246040_25kV_200600A__Multiple_Junction.pdf",
        video           : "", 
      )); 
      //PAGINA 3 008
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV, Insulating Plug",
        partnumber      : "25-IP600",
        campopesquisa   : "$categoria 25 kV, Connecting Plug Threaded Stud Compression Connector Cable Adapter Shear Bolted Connector 25-DIC200 25-IP600C 25-DCP 25-STUD 600BMC 25-ADP SBC 625BIP K650BIP DIP625A",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/1525-600A-Replacement_part.pdf",
        instrucao       : "",
        video           : "", 
      )); 
      //PAGINA 3 009
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "35 kV, Insulating Plug",
        partnumber      : "35-IP600",
        campopesquisa   : "$categoria 35 kV, Insulating Plug 35 kV, Connecting Plug Threaded Stud Compression Connector Cable Adapter Shear Bolted Connector 35-IP600 35-DCP 35-STUD 600BMC 35-ADP SBC",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/35-600A-Replacement_part.pdf",
        instrucao       : "",
        video           : "", 
      )); 
      //PAGINA 3 010
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "35 kV, 600A, Deadbreak T-Body",
        partnumber      : "35-TB600",
        campopesquisa   : "$categoria 35 kV, 600A, Deadbreak T-Body 35-TB600",
        assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/35TB600.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/35-TBODY-_INSTALLATION_SHEET-170921.pdf",
        video           : "", 
      )); 
      //PAGINA 3 011
      pagina=3;
      posicao++;
      //categoria = "IEEE/ANSI 600A Deadbreak";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "35 kV, 600A Deadbreak Junction",
        partnumber      : "35-DJ600",
        campopesquisa   : "$categoria 35 kV, 600A Deadbreak Junction 35-DJ600",
        assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/35DJ600.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A0740041_25-35kV_600A_JUNCTION_Instructions.pdf",
        video           : "", 
      ));                                     
    }
    posicao = 0;




    //PAGINA 4 001
    pagina=4;
    posicao++;
    //categoria = "IEC Interface A";
    categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    if(indice=="0" || indice == "4")
    {
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "24kV 250A Insulated Protective Cap",
        partnumber      : "24-DIC250",
        campopesquisa   : "$categoria 24-DIC250 24kV 250A Insulated Protective Cap",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24DIC250.pdf",
        instrucao       : "",
        video           : "", 
      ));                                     
      //PAGINA 4 002
      pagina=4;
      posicao++;
      //categoria = "IEC Interface A";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "17.5 kV / 24 kV 250A Straight Connector",
        partnumber      : "24-CL250",
        campopesquisa   : "$categoria 17.5 kV / 24 kV 250A Straight Connector 24-CL250",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24CL250.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/IEC/24CL250-INSTRUCTION_SHEET-20161117.pdf",
        video           : "", 
      ));                  
      //PAGINA 4 003
      pagina=4;
      posicao++;
      //categoria = "IEC Interface A";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "17.5 kV / 24 kV 250A Elbow Connector",
        partnumber      : "24-CE250",
        campopesquisa   : "$categoria 17.5 kV / 24 kV 250A Elbow Connector 24-CE250",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24CE250.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/IEC/24CE250-INSTRUCTION_SHEET-20161117.pdf",
        video           : "", 
      ));                  
      //PAGINA 4 004
      pagina=4;
      posicao++;
      //categoria = "IEC Interface A";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "24kV 250A Deadbreak Bushing Insert",
        partnumber      : "24-DBI250",
        campopesquisa   : "$categoria 24kV 250A Deadbreak Bushing Insert 24-DBI250",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24DBI250.pdf",
        instrucao       : "",
        video           : "", 
      ));    
    }
    posicao=0;       


    //PAGINA 5 001
    pagina=5;
    posicao++;
    //categoria = "IEC Interface B";
    categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    if(indice=="0" || indice == "5")
    {
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "36kV 400A Front T-body",
        partnumber      : "36-FDT400",
        campopesquisa   : "$categoria 36kV 400A Front T-body 36-FDT400",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/36FDT400.pdf",
        instrucao       : "",
        video           : "", 
      ));   
    }

    posicao=0;
    //PAGINA 6 001
    pagina=6;
    posicao++;
    //categoria = "IEC Interface C";
    categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    if(indice=="0" || indice == "6")
    {
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "17 kV / 50 kV Coupling T Surge Arrester",
        partnumber      : "17-RDTA50",
        campopesquisa   : "$categoria 17 kV / 50 kV Coupling T Surge Arrester 17-RDTA50",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/17RDTA50.pdf",
        instrucao       : "",
        video           : "", 
      ));       
      //PAGINA 6 002
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "26 kV / 66 kV Coupling T Surge Arrester	",
        partnumber      : "26-RDTA66",
        campopesquisa   : "$categoria 26 kV / 66 kV Coupling T Surge Arrester	26-RDTA66",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/26RDTA66.pdf",
        instrucao       : "",
        video           : "", 
      ));       
      //PAGINA 6 003
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "17.5 kV / 24 kV 630A Front T-Body",
        partnumber      : "24-FDT630",
        campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Front T-Body 24-FDT630",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24FDT630-RDT630.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/9-135004P010-17.5%2624kV_630A_IEC_T-body_Installation_Instructions.pdf",
        video           : "", 
      ));       
      //PAGINA 6 004
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "17.5 kV / 24 kV 630A Rear T-Body",
        partnumber      : "24-RDT630",
        campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Rear T-Body 24-RDT630",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24FDT630-RDT630.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/9-135004P010-17.5%2624kV_630A_IEC_T-body_Installation_Instructions.pdf",
        video           : "", 
      ));       
      //PAGINA 6 005
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "17.5 kV / 24 kV 630A Large Front T-Body",
        partnumber      : "24-LFDT630",
        campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Large Front T-Body 24-LFDT630",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24LFDT630-24LRDT630.pdf",
        instrucao       : "",
        video           : "", 
      ));       
      //PAGINA 6 006
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "17.5 kV / 24 kV 630A Large Rear T-Body",
        partnumber      : "24-LRDT630",
        campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Large Rear T-Body 24-LRDT630",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24LFDT630-24LRDT630.pdf",
        instrucao       : "",
        video           : "", 
      ));       
      //PAGINA 6 007
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "24kV 630A Insulated Protective Cap",
        partnumber      : "24-DIC630",
        campopesquisa   : "$categoria 24kV 630A Insulated Protective Cap 24-DIC630",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24-DIC630.pdf",
        instrucao       : "",
        video           : "", 
      ));       
      //PAGINA 6 008
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "34 kV / 85 kV Coupling T Surge Arrester	",
        partnumber      : "34-RDTA85",
        campopesquisa   : "$categoria 34 kV / 85 kV Coupling T Surge Arrester	34-RDTA85",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/34RDTA85.pdf",
        instrucao       : "",
        video           : "", 
      ));       
      //PAGINA 6 009
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "36 kV 630A Front T-Body",
        partnumber      : "36-FDT630",
        campopesquisa   : "$categoria 36 kV 630A Front T-Body 36-FDT630",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/36FDT630-RDT630.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/IEC/36FDT630%2636RDT630-Instruction_Sheet-20180404.pdf",
        video           : "", 
      ));       
      //PAGINA 6 010
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-0$posicao",
        categoria       : "$categoria",
        descricao       : "36 kV 630A Rear T-Body",
        partnumber      : "36-RDT630",
        campopesquisa   : "$categoria 36 kV 630A Rear T-Body 36-RDT630",
        assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/36FDT630-RDT630.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/IEC/36FDT630%2636RDT630-Instruction_Sheet-20180404.pdf",
        video           : "", 
      ));       
      //PAGINA 6 011
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-0$posicao",
        categoria       : "$categoria",
        descricao       : "42 kV 1250A Front T-Body",
        partnumber      : "42-FDT1250",
        campopesquisa   : "$categoria 42 kV 1250A Front T-Body 42-FDT1250",
        assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/42FDT1250-RDT1250.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/42kV_1250A_FDT_RDT_installation_instruction_CHARDON.pdf",
        video           : "", 
      ));       
      //PAGINA 6 012
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-0$posicao",
        categoria       : "$categoria",
        descricao       : "42 kV 1250A Rear T-Body",
        partnumber      : "42-RDT1250",
        campopesquisa   : "$categoria 42 kV 1250A Rear T-Body 42-RDT1250",
        assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/42FDT1250-RDT1250.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/42kV_1250A_FDT_RDT_installation_instruction_CHARDON.pdf",
        video           : "", 
      ));       
      //PAGINA 6 013
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-0$posicao",
        categoria       : "$categoria",
        descricao       : "42 kV 1250A Bushing Extender",
        partnumber      : "42-BE1250",
        campopesquisa   : "$categoria 42 kV 1250A Bushing Extender 42-BE1250",
        assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/42-BE1250.pdf",
        instrucao       : "",
        video           : "", 
      ));       
      //PAGINA 6 014
      pagina=6;
      posicao++;
      //categoria = "IEC Interface C";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-0$posicao",
        categoria       : "$categoria",
        descricao       : "51 kV / 134 kV Coupling T Surge Arrester",
        partnumber      : "51-RDTA134",
        campopesquisa   : "$categoria 51 kV / 134 kV Coupling T Surge Arrester 51-RDTA134",
        assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/51RDTA134.pdf",
        instrucao       : "",
        video           : "", 
      ));       
    }

    posicao=0;
    //PAGINA 7 001
    pagina=7;
    posicao++;
    //categoria = "Transformer Components";
    categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    if(indice=="0" || indice == "7")
    {
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "Sidewall/Cover Mounted Fuse Holder Assembly",
        partnumber      : "CHBON Series",
        campopesquisa   : "$categoria Sidewall/Cover Mounted Fuse Holder Assembly CHBON Series",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/TRANSFORMER/CHBON.pdf",
        instrucao       : "",
        video           : "", 
      ));       
      //PAGINA 7 002
      pagina=7;
      posicao++;
      //categoria = "Transformer Components";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "Chardon 15, 25, 28 kV 200A Bushing Well",
        partnumber      : "CH200BW",
        campopesquisa   : "$categoria Chardon 15, 25, 28 kV 200A Bushing Well CH200BW",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/CH200BW.pdf",
        instrucao       : "",
        video           : "", 
      ));       
      //PAGINA 7 003
      pagina=7;
      posicao++;
      //categoria = "Transformer Components";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "Isolation Link",
        partnumber      : "CHIL",
        campopesquisa   : "$categoria Isolation Link CHIL",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/TRANSFORMER/CHIL.pdf",
        instrucao       : "",
        video           : "", 
      ));           
    }

    posicao=0;
    //PAGINA 8 001
    pagina=8;
    posicao++;
    //categoria = "Epoxy Products";
    if(indice=="0" || indice == "8")
    {
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV 200A Lodabreak Integral Bushing",
        partnumber      : "15-LIB130 15-LIB185",
        campopesquisa   : "$categoria 15 kV 200A Lodabreak Integral Bushing 15-LIB130 15-LIB185",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/EXPOXYctconly/15LIB130-LIB185.pdf",
        instrucao       : "",
        video           : "", 
      ));   
      //PAGINA 8 002
      pagina=8;
      posicao++;
      //categoria = "Epoxy Products";
      categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "17.5 kV / 24 kV 630A Apparatus Bushing",
        partnumber      : "24-TCP630 36-TCP630 42-TCP1250",
        campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Apparatus Bushing 24-TCP630 36-TCP630 42-TCP1250",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/EXPOXYctconly/24TCP.pdf",
        instrucao       : "",
        video           : "", 
      ));   
      //PAGINA 8 003
      pagina=8;
      posicao++;
      //categoria = "Epoxy Products";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "25 kV Deadbreak 600A/900A/Insulated Plug",
        partnumber      : "25-IP600 25-DCP600 25-DCP900",
        campopesquisa   : "$categoria 25 kV Deadbreak Insulated Plug 25kV Deadbreak 600A Connecting Plug 25 kV Deadbreak 900A Connecting Plug 25-IP600 25-IP600 25-DCP600 25-DCP900",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/1525-600A-Replacement_part.pdf",
        instrucao       : "",
        video           : "", 
      ));   
      //PAGINA 8 004
      pagina=8;
      posicao++;
      //categoria = "Epoxy Products";
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "35 kV Deadbreak 600A/900A/Insulated Plug",
        partnumber      : "35-IP600 35-DCP600 35-DCP900",
        campopesquisa   : "$categoria 35-IP600 35-DCP600 35-DCP900 35 kV Deadbreak Insulated Plug 35kV Deadbreak 600A Connecting Plug 35 kV Deadbreak 900A Connecting Plug",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/35-600A-Replacement_part.pdf",
        instrucao       : "",
        video           : "", 
      ));   
    }

    posicao=0;
    //PAGINA 9 001
    pagina=9;
    posicao++;
    //categoria = "Switchgear Products";
    categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    if(indice=="0" || indice == "9")
    {
      itemsChardom.add(ItemChardon(
        id              : "P00$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "Chardon Epoxy Solid Recloser",
        partnumber      : "CKGR",
        campopesquisa   : "$categoria Chardon Epoxy Solid Recloser CKGR",
        assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/Switchgear/CKGR.pdf",
        instrucao       : "",
        video           : "", 
      ));   
    }


    posicao=0;
    //PAGINA 10 001
    pagina=10;
    posicao++;
    //categoria = "Cold Shrinkable Products";
    categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    if(indice=="0" || indice == "10")
    {
      itemsChardom.add(ItemChardon(
        id              : "P0$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "15 kV, 25 kV and 35kV Cold Shrinkable Termination",
        partnumber      : "15/25-CSTO",
        campopesquisa   : "$categoria 15 kV, 25 kV and 35kV Cold Shrinkable Termination 15-CSTO 25-CSTO",
        assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/ColdShrinkCTConly/152535CSTO.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/ColdShrinK/15%2625kVCSTO-Installation_sheet_-20190315.PDF",
        video           : "", 
      ));   
      //PAGINA 10 002
      pagina=10;
      posicao++;
      //categoria = "Cold Shrinkable Products";
      itemsChardom.add(ItemChardon(
        id              : "P0$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "35kV Cold Shrinkable Termination",
        partnumber      : "35-CSTO",
        campopesquisa   : "$categoria 35kV Cold Shrinkable Termination 35-CSTO",
        assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/ColdShrinkCTConly/152535CSTO.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/ColdShrinK/35kVCSTO-Installation_sheet_-20190315.PDF",
        video           : "", 
      ));     
      //PAGINA 10 003
      pagina=10;
      posicao++;
      //categoria = "Cold Shrinkable Products";
      itemsChardom.add(ItemChardon(
        id              : "P0$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "Cold Shrinkable Jacket Seal",
        partnumber      : "CJS",
        campopesquisa   : "$categoria Cold Shrinkable Jacket Seal CJS",
        assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/ColdShrinkCTConly/CJS.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/ColdShrinK/CJS-INSTRUCTION_SHEET-20181222_.pdf",
        video           : "", 
      ));       
    }

    posicao=0;
    //PAGINA 11 001
    pagina=11;
    posicao++;
    //categoria = "Other Connectors";
    categoria = gblIdiomasIndice[indiceIdioma]["categoria$pagina"];
    if(indice=="0" || indice == "11")
    {
      itemsChardom.add(ItemChardon(
        id              : "P0$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "Shear Bolt Connector",
        partnumber      : "SBC",
        campopesquisa   : "$categoria Shear Bolt Connector SBC",
        assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/OtherCTConlly/SBC.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/OTHER/Shear_Bolt_Connector-Installation_Instruction.pdf",
        video           : "", 
      ));   
      //PAGINA 11 002
      pagina=11;
      posicao++;
      //categoria = "Other Connectors";
      itemsChardom.add(ItemChardon(
        id              : "P0$pagina-00$posicao",
        categoria       : "$categoria",
        descricao       : "Submersible Low Voltage Connector",
        partnumber      : "SLVC - BMI",
        campopesquisa   : "$categoria Submersible Low Voltage Connector SLVC BMI",
        assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
        assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/OtherCTConlly/SLVC.pdf",
        instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/OTHER/SLVC_instruction_sheet.pdf",
        video           : "", 
      ));   
      //PAGINA 11 003
      pagina=11;
      posicao++;
      //categoria = "Other Connectors";
      itemsChardom.add(ItemChardon(
        id              : "P011-003",
        categoria       : "$categoria",
        descricao       : "T-Wrench 5/16-inch Hex Wrench",
        partnumber      : "CT-TW001",
        campopesquisa   : "$categoria T-Wrench 5/16-inch Hex Wrench CT-TW001",
        assetImagem     : "assets/products/P011/003.jpg",
        assetImagemMini : "assets/products/P011/mini/003.png",
        catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/OtherCTConlly/CTTW001.pdf",
        instrucao       : "",
        video           : "", 
      ));           
    }
    return itemsChardom;
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _searchBar(),
        actions: <Widget>[
          Icon(Icons.search),
        ],
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return index == 0 ? _none() : _listItem(index-1);
        },
        itemCount: _notesForDisplay.length+1,
      ),
    );
  }

  _none() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
    );
  }

  _searchBar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        decoration: InputDecoration(
          hintStyle: TextStyle(color: Colors.white60),
          hintText: gblIdiomasIndice[indiceIdioma]["procurar"],          
        ),
        style: TextStyle(color: Colors.white),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            _notesForDisplay = _notes.where((note) {
              var noteTitle = note.campopesquisa.toLowerCase();
              return noteTitle.contains(text);
            }).toList();
          });
        },
      ),
    );
  }

  _launchURL(endereco) async {
    if (await canLaunch(endereco)) {
      await launch(endereco);
    } else {
      throw 'Could not launch $endereco';
    }
  }   


  _listItem(index) {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0, bottom: 32.0, left: 16.0, right: 16.0),
        child: 
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        child: Image.asset(_notesForDisplay[index].assetImagemMini,width: 80,),
                        onTap: () {
                          Navigator.push(
                            context, MaterialPageRoute(
                              builder: (context) => Imagem(_notesForDisplay[index].assetImagem, _notesForDisplay[index].descricao),
                          ));
                        },
                      ),
                      InkWell(
                        child: Image.asset("assets/images/catalog.png",width: 80,),
                        onTap: () {
                          //Navigator.push(
                          //  context, MaterialPageRoute(
                          //    builder: (context) => PDF(_notesForDisplay[index].catalogo),
                          //));
                          loadPDFFromUrl(_notesForDisplay[index].catalogo);
                        },
                      ),
                      InkWell(
                        onTap: () {
                          //Navigator.push(
                          //  context, MaterialPageRoute(
                          //    builder: (context) => PDFUrl(_notesForDisplay[index].instrucao),//_launchURL(_notesForDisplay[index].instrucao),//PDF(_notesForDisplay[index].instrucao),
                          //));
                          loadPDFFromUrl(_notesForDisplay[index].instrucao);
                        },
                        child: _notesForDisplay[index].instrucao!=""
                          ? Image.asset("assets/images/instruction.png",width: 80,)
                          : Image.asset("assets/images/blank.png",width: 80,)
                      ),
                      _notesForDisplay[index].video != ""
                        ?InkWell(
                        onTap: () {
                          _launchURL(_notesForDisplay[index].video);
                        },
                        child:Icon(Icons.video_library, size: 40, color: Colors.redAccent,),
                      )
                      :Text("")
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Center(
                  child: Text(
                    _notesForDisplay[index].categoria,
                    style: TextStyle(
                      fontSize: 22,
                      color: Colors.blue,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    _notesForDisplay[index].descricao,
                    style: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                SizedBox(height: 5,),
                Center(
                  child: Text(
                    _notesForDisplay[index].partnumber,
                    style: TextStyle(
                      color: Colors.grey.shade600,
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
      ),
    );
  }  
}

//PÁGINA DE ANIMÇÃO
class Starter extends StatefulWidget {
  @override
  _StarterState createState() => _StarterState();
}
class _StarterState extends State<Starter> with TickerProviderStateMixin{
  AnimationController _scaleController;
  AnimationController _scale2Controller;
  AnimationController _widthController;
  AnimationController _positionController;

  Animation<double> _scaleAnimation;
  Animation<double> _scale2Animation;
  Animation<double> _widthAnimation;
  Animation<double> _positionAnimation;

  bool hideIcon = false;

  @override
  void initState() {
    super.initState();

    _scaleController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300)
    );

    _scaleAnimation = Tween<double>(
      begin: 1.0, end: 0.8
    ).animate(_scaleController)..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _widthController.forward();
      }
    });

    _widthController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 600)
    );

    _widthAnimation = Tween<double>(
      begin: 80.0,
      end: 300.0
    ).animate(_widthController)..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _positionController.forward();
      }
    });

    _positionController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000)
    );

    _positionAnimation = Tween<double>(
      begin: 0.0,
      end: 215.0
    ).animate(_positionController)..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          hideIcon = true;
        });
        Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: ListaProdutosCategorias()));
        //_scale2Controller.forward();
      }
    });

    _scale2Controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000)
    );

    _scale2Animation = Tween<double>(
      begin: 1.0,
      end: 32.0
    ).animate(_scale2Controller)..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        //Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: LoginPage()));
        Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: ListaProdutos("0")));
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color.fromRGBO(3, 9, 23, 1),
      body: Container(
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            Positioned(
              top: -50,
              left: 0,
              child: FadeAnimation(1, Container(
                width: width,
                height: 400,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/one.png'),
                    fit: BoxFit.cover
                  )
                ),
              )),
            ),
            Positioned(
              top: -100,
              left: 0,
              child: FadeAnimation(1.3, Container(
                width: width,
                height: 400,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/one.png'),
                    fit: BoxFit.cover
                  )
                ),
              )),
            ),
            Positioned(
              top: -150,
              left: 0,
              child: FadeAnimation(1.6, Container(
                width: width,
                height: 400,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/one.png'),
                    fit: BoxFit.cover
                  )
                ),
              )),
            ),
            Container(
              padding: EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      FadeAnimation(2, Text("Chardon", 
                      style: TextStyle(color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold),)),
                      FadeAnimation(2, Text(" Group", 
                      style: TextStyle(color: Colors.white, fontSize: 35),)),
                    ],
                  ),
                  FadeAnimation(1, Text(gblIdiomasIndice[indiceIdioma]["bemVindo"], 
                  style: TextStyle(color: Colors.white, fontSize: 50),)),
                  SizedBox(height: 15,),
                  FadeAnimation(1.3, Text(gblIdiomasIndice[indiceIdioma]["bemVindoL1"] + "\n" + gblIdiomasIndice[indiceIdioma]["bemVindoL2"], 
                  style: TextStyle(color: Colors.white.withOpacity(.7), height: 1.4, fontSize: 20),)),
                  SizedBox(height: 180,),
                  FadeAnimation(1.6, AnimatedBuilder(
                    animation: _scaleController,
                    builder: (context, child) => Transform.scale(
                    scale: _scaleAnimation.value,
                    child: Center(
                      child: AnimatedBuilder(
                        animation: _widthController,
                        builder: (context, child) => Container(
                          width: _widthAnimation.value,
                          height: 80,
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.blue.withOpacity(.4)
                          ),
                          child: InkWell(
                            onTap: () {
                              _scaleController.forward();
                            },
                            child: Stack(
                              children: <Widget> [
                                AnimatedBuilder(
                                  animation: _positionController,
                                  builder: (context, child) => Positioned(
                                    left: _positionAnimation.value,
                                    child: AnimatedBuilder(
                                      animation: _scale2Controller,
                                      builder: (context, child) => Transform.scale(
                                        scale: _scale2Animation.value,
                                        child: Container(
                                          width: 60,
                                          height: 60,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.blue
                                          ),
                                          child: hideIcon == false ? Icon(Icons.arrow_forward, color: Colors.white,) : Container(),
                                        )
                                      ),
                                    ),
                                  ),
                                ),
                              ]
                            ),
                          ),
                        ),
                      ),
                    )),
                  )),
                  SizedBox(height: 60,),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
//FIM DA PÁGINA DE ANIMAÇÃO

//FADE ANIMATION
class FadeAnimation extends StatelessWidget {
  final double delay;
  final Widget child;

  FadeAnimation(this.delay, this.child);

  @override
  Widget build(BuildContext context) {
    final tween = MultiTrackTween([
      Track("opacity").add(Duration(milliseconds: 500), Tween(begin: 0.0, end: 1.0)),
      Track("translateY").add(
        Duration(milliseconds: 500), Tween(begin: -130.0, end: 0.0),
        curve: Curves.easeOut)
    ]);

    return ControlledAnimation(
      delay: Duration(milliseconds: (500 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Transform.translate(
          offset: Offset(0, animation["translateY"]), 
          child: child
        ),
      ),
    );
  }
}
//FIM FADEANIMATION


//PAGINA PERFIL
class Perfil extends StatefulWidget {
  @override
  _PerfilState createState() => _PerfilState();
}
class _PerfilState extends State<Perfil> {

TextEditingController controllerNome = TextEditingController();
TextEditingController controllerEmail = TextEditingController();
TextEditingController controllerEmpresa = TextEditingController();
TextEditingController controllerSenha = TextEditingController();
TextEditingController controllerConfirmacao = TextEditingController();



  String _nome, _email, _empresa, _senha, _confirmacao;
  final formKey = GlobalKey<FormState>();


   @override
  void initState() {
    loadFromDB();
    super.initState();
  }  
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text(gblIdiomasIndice[indiceIdioma]["perfil"]),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.check_circle),
              onPressed: (){
                saveToDB(_nome, _email, _empresa, _senha, _confirmacao);
                Navigator.pop(context);
              },
            ),
          ],
        ),
        backgroundColor: Colors.white,
        body: Padding( 
          padding: EdgeInsets.all(10), 
          child: Form(
            key: formKey,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  TextField(
                    autofocus: true,
                    controller: controllerNome,
                    maxLength: 50,
                    textCapitalization: TextCapitalization.characters,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gblIdiomasIndice[indiceIdioma]["perfil_nome"]),
                    onChanged: (value) {
                      _nome = value;
                    },
                  ),
                  TextField(
                    textCapitalization: TextCapitalization.none,
                    controller: controllerEmail,
                    maxLength: 50,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gblIdiomasIndice[indiceIdioma]["perfil_email"]),
                    onChanged: (value) {
                      _email = value;
                    },
                  ),
                  TextField(
                    textCapitalization: TextCapitalization.characters,
                    controller: controllerEmpresa,
                    maxLength: 50,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gblIdiomasIndice[indiceIdioma]["perfil_empresa"]),
                    onChanged: (value) {
                      _empresa = value;
                    },
                  ),
                  TextField(
                    obscureText: true,
                    controller: controllerSenha,
                    maxLength: 15,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gblIdiomasIndice[indiceIdioma]["perfil_senha"]),
                    onChanged: (value) {
                      _senha = value;
                    },
                  ),
                  TextField(
                    obscureText: true,
                    controller: controllerConfirmacao,
                    maxLength: 15,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gblIdiomasIndice[indiceIdioma]["perfil_confirmacao"]),
                    onChanged: (value) {
                      _confirmacao = value;
                    },
                  ),
                  Icon(Icons.person_pin, size: 150, color: Colors.blue,),
                ],
              ),
          ),
          ),
        ),
      );
  }

  Future saveToDB(nome, email, empresa, senha, confirmacao) async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { 
    },
    onCreate: (Database db, int version) async {
    });

    //DELETE
    //await database.rawDelete(
    //  'DELETE FROM cfg',
    //);

    //INSERT
    if(senha == confirmacao && senha != null) {
      print(nome);
      print(email);
      print(empresa);
      print(senha);
      await database.rawInsert(
        'UPDATE profile SET nome=?, email=?, empresa=?, senha=?', 
        [nome, email, empresa, senha]
      );        
    } else {
      return false;
    }
  }

  loadFromDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { },
    onCreate: (Database db, int version) async {
    });

    //DELETE
    //await database.rawDelete(
    //  'DELETE FROM cfg',
    //);

    //INSERT
    //await database.rawInsert(
    //  'INSERT INTO cfg (idioma) values (?)', [1]
    //);        

    //SELECT
    var lista = await database.query("profile", 
      columns: ["nome", "email", "empresa", "senha"],
    );    
    if(lista.length == 0){
      setState(() {
        _nome = "";
        _email = "";
        _empresa = "";
        _senha = "";
        _confirmacao = ""; 

        controllerNome.text = _nome;
        controllerEmail.text = _email;
        controllerEmpresa.text = _empresa;
        controllerSenha.text = _senha;
        controllerConfirmacao.text = _confirmacao;
      });      
    } else {    
      for(var item in lista){
        setState(() {
          _nome = item["nome"];
          _email = item["email"];
          _empresa = item["empresa"];
          _senha = item["senha"];
          _confirmacao = item["senha"]; 

          controllerNome.text = _nome;
          controllerEmail.text = _email;
          controllerEmpresa.text = _empresa;
          controllerSenha.text = _senha;
          controllerConfirmacao.text = _confirmacao;
        });
      }
    }
  }
}
// FIM PAGINA PERFIL


//PAGINA
class SelecionaIdioma extends StatefulWidget {
  @override
  _SelecionaIdiomaState createState() => _SelecionaIdiomaState();
}
class _SelecionaIdiomaState extends State<SelecionaIdioma> {

  int meuIndice;

  @override
  void initState() {
    _executar();
    super.initState();
  }  
  void _executar() async{
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { },
    onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE cfg (idioma integer)");
      await db.execute("CREATE TABLE profile (nome TEXT, email TEXT, empresa TEXT, senha TEXT)");
    });

    //DELETE
    //await database.rawDelete(
    //  'DELETE FROM cfg',
    //);

    //INSERT
    //await database.rawInsert(
    //  'INSERT INTO cfg (idioma) values (?)', [1]
    //);        

    //SELECT
    var lista = await database.query("cfg", 
      columns: ["idioma"],
      where: "idioma>=?",
      whereArgs: ["0"]
    );
    /*print("LISTA:");
    print(lista);
    if(lista.length == 0){
      print("igual");
    } else {
      print("diferente");
    }*/
    if(lista.length==0){
      await database.rawInsert(
        'INSERT INTO cfg (idioma) values (?)', [1]
      );  
      await database.rawInsert(
        'INSERT INTO profile (nome, email, empresa, senha) values ( ?, ?, ?, ?)', 
        ["", "", "", ""]
      );      
      setState(() {
        indiceIdioma = 1;
        meuIndice = 1;
      });            
    }    
    for(var item in lista){
      setState(() {
        indiceIdioma = item["idioma"];
        meuIndice = item["idioma"];
      });
    }
  }


  void _trocarIdioma() async{
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { },
    onCreate: (Database db, int version) async {
    });

    await database.rawUpdate(
      'UPDATE cfg SET idioma=?',
      [this.meuIndice]
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(gblIdiomasIndice[indiceIdioma]["idiomaPreferencia"]),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: InkWell(
              onTap: (){
                this.meuIndice = 0;
                setState(() {
                  _trocarIdioma();
                });
                //Navigator.pop(context);
                Navigator.of(context).pushNamed("/");
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/languages/01.png", width: 80,),
                      SizedBox(width: 10,),
                      Text(gblIdiomas[0], style: TextStyle(fontSize: 20),),
                    ],
                  ),
                  this.meuIndice == 0?Icon(Icons.check_box):Icon(Icons.check_box_outline_blank)
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              onTap: (){
                this.meuIndice = 1;
                setState(() {
                  _trocarIdioma();
                });
                //Navigator.pop(context);
                Navigator.of(context).pushNamed("/");
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/languages/02.png", width: 80,),
                      SizedBox(width: 10,),
                      Text(gblIdiomas[1], style: TextStyle(fontSize: 20),),
                    ],
                  ),
                  this.meuIndice == 1?Icon(Icons.check_box):Icon(Icons.check_box_outline_blank)
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              onTap: (){
                this.meuIndice = 2;
                setState(() {
                  _trocarIdioma();
                });
                //Navigator.pop(context);
                Navigator.of(context).pushNamed("/");
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/languages/03.png", width: 80,),
                      SizedBox(width: 10,),
                      Text(gblIdiomas[2], style: TextStyle(fontSize: 20),),
                    ],
                  ),
                  this.meuIndice == 2?Icon(Icons.check_box):Icon(Icons.check_box_outline_blank)
                ],
              ),

            ),
          ),
          Card(
            child: InkWell(
              onTap: (){
                this.meuIndice = 3;
                setState(() {
                  _trocarIdioma();
                });
                //Navigator.pop(context);
                Navigator.of(context).pushNamed("/");
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/languages/04.png", width: 80,),
                      SizedBox(width: 10,),
                      Text(gblIdiomas[3], style: TextStyle(fontSize: 20),),
                    ],
                  ),
                  this.meuIndice == 3?Icon(Icons.check_box):Icon(Icons.check_box_outline_blank)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
// FIM PAGINA SELECIONA IDIOMA